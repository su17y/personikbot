package com.personik.controller;

import com.personik.service.SearchService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class SearchController {
    
    
    private final SearchService searchService;

    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping(value = "find/{query}")
    public String getFirstUrl(@PathVariable String query) {
        return searchService.getUrl(query,0);
    }

    @GetMapping(value = "find/{query}/{number}")
    public String getSpecificUrl(@PathVariable String query, @PathVariable int number){
        return searchService.getUrl(query,number - 1);
    }
}
