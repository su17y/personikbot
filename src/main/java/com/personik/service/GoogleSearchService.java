package com.personik.service;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.CustomsearchRequestInitializer;
import com.google.api.services.customsearch.model.Search;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.GeneralSecurityException;


@Component
public class GoogleSearchService implements SearchService{


    private final String cx = "015767568282658350123:rmshwuomfup";

    private final String key = "AIzaSyBRgjAdHwpopg-wsnyjhjwEfGylvjVfrA0";

    private final String applicationName = "MyApplication";

    private Customsearch cs;

    public GoogleSearchService(){
        try {
            cs = new Customsearch.Builder(GoogleNetHttpTransport.newTrustedTransport(),
                    JacksonFactory.getDefaultInstance(), null)
                    .setApplicationName(applicationName)
                    .setGoogleClientRequestInitializer(
                            new CustomsearchRequestInitializer(key))
                    .build();
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getUrl(String query, int number) {
        Customsearch.Cse.List list = null;
        try {
            list = cs.cse().list(query).setCx(cx);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Search result = null;
        try {
            result = list.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(number >= result.getItems().size())
            return "No link with this index number!";
        return result.getItems().get(number).getTitle() + ", " + result.getItems().get(number).getLink();
    }


}
