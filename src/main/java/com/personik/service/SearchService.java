package com.personik.service;


public interface SearchService {

    String getUrl(String query, int number);

}
